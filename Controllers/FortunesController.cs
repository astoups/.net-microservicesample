﻿
using System.Collections.Generic;

using Microsoft.AspNetCore.Mvc;
using System;

namespace FortuneTellerService.Controllers
{
    [Route("api/[controller]")]
    public class FortunesController : Controller
    {

        // POST: api/fortunes/mentionShuffle
        [HttpPost("mentionShuffle")]
        public Mention[] Shuffle([FromBody]Mention[] req)
        {

            char[] delimiterChars = { ' ', ',', '.', ':', '\t' };

            foreach ( Mention m in req)
            {
                string[] words = m.Text.Split(delimiterChars);
                words = reshuffle(words);
                m.Text = string.Join(" ", words);
            }

            return req;
        }

        string[] reshuffle(string[] items)
        {
            Random rand = new Random();

            int removeNum = (int)((double)items.Length * 0.3);
            int keepTotal = items.Length - removeNum;

            List <string> itemsList = new List<string>();
            itemsList.AddRange(items);

            for (int num = 0; num < removeNum; num++)
            {
                    int index = rand.Next(0, itemsList.Count);
                    itemsList.RemoveAt(index);
            }

            var newItems = itemsList.ToArray();

            for (int i = 0; i < newItems.Length - 1; i++)
            {
                int j = rand.Next(i, newItems.Length);
                string temp = newItems[i];
                newItems[i] = newItems[j];
                newItems[j] = temp;

            }

            return newItems;
        }


    }
}
