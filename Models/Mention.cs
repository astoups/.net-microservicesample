﻿namespace FortuneTellerService.Controllers
{
    public class Mention
    {
        public Mention(string text)
        {
            this.Text = text;
        }

        public string Text { get; set; }
    }
}