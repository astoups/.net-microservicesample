# README #

Some Useful References:

Wiki explaining the Steeltoe framework for .NET service discovery:
http://steeltoe.io/docs/steeltoe-discovery/#1-0-netflix-eureka
Steeltoe Sample we based our example on:
https://github.com/SteeltoeOSS/Samples/tree/master/Discovery/src/AspDotNet4/Fortune-Teller-Service4
Netflix Eureka Wiki:
https://github.com/Netflix/eureka/wiki
Spring Cloud Eureka we will be using
https://cloud.spring.io/spring-cloud-netflix/
